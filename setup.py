"""
USAGE: 
   o install in develop mode: navigate to the folder containing this file,
                              and type 'python setup.py develop --user'.
                              (ommit '--user' if you want to install for 
                               all users)                           
"""
from setuptools import setup

setup(name='afines_reader',
      version='0.0.1',
      description='Read files generated with AFINES framework',
      url='',
      author='Ilyas Kuhlemann',
      author_email='ilyasp.ku@gmail.com',
      license='MIT',
      packages=["afines_reader"],
      entry_points={
          "console_scripts": [              
          ],
          "gui_scripts": [
          ]
      },
      install_requires=['numpy'],
      zip_safe=False)
