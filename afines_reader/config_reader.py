
from configparser import ConfigParser
import numpy as np

class ConfigReader(object):

    def __init__(self, content_of_config_file: str):
        self.config_dict = self._get_config_as_dict(content_of_config_file)

    def _get_config_as_dict(self, content_of_config_file: str):
        config_parser = ConfigParser()
        
        config_parser.read_string("[DUMMY]\n" + content_of_config_file)
        return config_parser["DUMMY"]

    def get_time_step(self) -> float:
        return float(self.config_dict["dt"])

    def get_box_size(self) -> np.ndarray:
        return np.array([float(self.config_dict["xrange"]),
                         float(self.config_dict["yrange"])])

    def get_bead_diameter(self):
        return float(self.config_dict["actin_length"])
